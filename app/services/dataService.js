(function() {
  'use strict'

  angular.module('app').
  factory('dataService', dataService);

  dataService.$inject = ['$http', '$q', '$state', 'api'];

  function dataService($http, $q,  $state, api) {

    var dataService = {
      getAll:getAll,
      getById: getById
    };

    return dataService;

    function getAll() {
      var deferred = $q.defer();

      $http.get(api)
        .success(function(data, status, headers, config) {
          deferred.resolve(data);
        })
        .error(function(status) {
          deferred.reject(status);
        });
      return deferred.promise;
    }

    function getById(spaceId) {
      var deferred = $q.defer();

      $http.get(api)
        .success(function(data, status, headers, config) {
          var result = _.find(data, function(o) {return o.id == spaceId; });
          deferred.resolve(result);
        })
        .error(function(status) {
          deferred.reject(status);
        });
      return deferred.promise;
    }
  }
})();
