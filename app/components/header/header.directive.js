(function() {
    'use strict';

    angular
        .module('app')
        .directive('headerDirective', headerDirective);

    function headerDirective() {
        var directive = {
            restrict: 'EA',
            templateUrl: 'app/components/header/header.html',
            controller: headerController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;
    }

    headerController.$inject = ['$state'];

    /* @ngInject */
    function headerController($state) {
        var vm = this;
        activate();
        function activate() {
          console.log('header Activate');
        }
    }
})();
