(function() {
    'use strict';

    angular
        .module('app')
        .controller('spaceController', spaceController);

    spaceController.$inject = ['dataService', '$stateParams', 'usSpinnerService'];

    /* @ngInject */
    function spaceController(dataService, $stateParams, usSpinnerService) {
        var vm = this;
        vm.spaceId = $stateParams.spaceId;
        activate();

        function activate() {
          console.log('Space View activate');
          dataService.getById(vm.spaceId).then(function(data){
            //Declaraci�n de Variables
            vm.data=data;
            vm.init=true;
            vm.service =['Internet Wi-Fi','Parking',' Coffee Machine', 'Easy Access', ' 24hrs. Security', 'Parking'];
            vm.cantidad =[];
            vm.services =[];

            //console.log(data);

            for (var i = 0, l = data.spaces[0].capacity; i < l; i++) {
                vm.cantidad.push({url:'assets/images/person.png'});
            }
            for (var i = 0, l = 4; i < l; i++) {
                vm.services.push({value:vm.service[i]});
            }
           console.log(vm.services);
           vm.init=false;
           usSpinnerService.stop('spinner-1');
          },
          function(err) {
            console.log("error", err);
          });
        }
    }
})();
