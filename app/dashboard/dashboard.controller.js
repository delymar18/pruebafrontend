(function() {
    'use strict';

    angular
        .module('app')
        .controller('dashboardController', dashboardController);

    dashboardController.$inject = ['dataService', 'Pagination', '$state', 'usSpinnerService'];

    /* @ngInject */
    function dashboardController(dataService, Pagination, $state, usSpinnerService) {
        var vm = this;
        vm.pagination = Pagination.getNew(4);
        vm.viewSpace = viewSpace;

        activate();

        function activate() {
          console.log('Dashboard View activate');
          dataService.getAll().then(function(data){
            vm.data= data;
            // PARA OBTENER LA CANTIDAD DE PAGINAS
            vm.pagination.numPages = Math.ceil(data.length/vm.pagination.perPage);
            usSpinnerService.stop('spinner-1');
          });
        }
        function viewSpace(id) {
          $state.go('space',{
            spaceId : id
          });
        }
    }
})();
