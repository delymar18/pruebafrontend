(function() {

    angular.module('app').config(appConfig);

    appConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    function appConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                views: {
                    "main": {
                        controller: 'dashboardController',
                        controllerAs: 'vm',
                        templateUrl: 'app/dashboard/dashboard.html'
                    }
                }
            })
            .state('space', {
                url: '/spaces/:spaceId',
                views: {
                    "main": {
                        controller: 'spaceController',
                        controllerAs: 'vm',
                        templateUrl: 'app/space/space.html'
                    }
                }
            })
            .state('register', {
                url: '/register',
                views: {
                    "main": {
                        controller: 'registerController',
                        controllerAs: 'vm',
                        templateUrl: 'app/register/register.html'
                    }
                }
            })
            .state('login', {
                url: '/login',
                views: {
                    "main": {
                        controller: 'loginController',
                        controllerAs: 'vm',
                        templateUrl: 'app/login/login.html'
                    }
                }
            });

        $urlRouterProvider.otherwise('/dashboard');
    }
})();
